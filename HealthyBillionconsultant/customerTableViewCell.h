//
//  customerTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 13/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *namelabel;
@property (strong, nonatomic) IBOutlet UILabel *phnnolabel;
@property (strong, nonatomic) IBOutlet UIImageView *arraoimageview;
@property (strong, nonatomic) IBOutlet UILabel *divider;

@end
