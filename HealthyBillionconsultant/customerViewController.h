//
//  customerViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 13/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "HBTabbarController.h"

#import "customerTableViewCell.h"

@interface customerViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;

@end
