//
//  invoicelistTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 19/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface invoicelistTableViewCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *itemname;
@property (strong, nonatomic) IBOutlet UILabel *itemprice;

@end
