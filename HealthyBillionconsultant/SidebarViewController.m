//
//  SidebarViewController.m
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Namecell.h"
#import "passwordViewController.h"


@interface SidebarViewController ()
{

    NSIndexPath *lastindexpath;
    AppDelegate *app;
    CGRect screenBounds;
}

@end

@implementation SidebarViewController
{
    NSArray *menuItems;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

// new one

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [scrollView setContentSize:CGSizeMake(0, 460)];
}



- (void)viewDidLoad
{
     [super viewDidLoad];
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.tableView.scrollEnabled=YES;
    
    
    
    menuItems = @[@"Name",@"Profile",@"Schedule", @"Invoices",@"PENDINGLOGS",@"COLLECTIONS",@"Social", @"Customer",@"myprogress",@"Logout",@"test"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

#pragma to stop selection
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // rows in section 0 should not be selectable
    if ( indexPath.row == 10||indexPath.row==0) return nil;
    
    // By default, allow row to be selected
    return indexPath;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%@",app->globalcustomerdetail);
    
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    if([CellIdentifier isEqualToString:@"Name"])
    {
       NSString *appendedpart=[NSString stringWithFormat:@" %@",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"]valueForKey:@"last_name"]];
       Namecell *cell1=(Namecell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       cell1.nameLabel.text=[[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"]valueForKey:@"first_name"] stringByAppendingString:appendedpart];
       cell1.contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0];
       cell=cell1;
    }
    else
    {
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    if(indexPath.row!=10&&indexPath.row!=0)
    {
    cell.contentView.backgroundColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];
    }
    
    if(lastindexpath)
    {
        if(indexPath.row!=10&&indexPath.row!=0)
        {
       // NSLog(@"%ld",(long)lastindexpath.row);
        if(lastindexpath.row==indexPath.row)
        {
           
          cell.contentView.backgroundColor = [UIColor colorWithRed:25/255.0f green:100/255.0f blue:168/255.0f alpha:1.0];
        }
        else
        {
         cell.contentView.backgroundColor = [UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];
        }
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
//    if(indexPath.row==9)
//    {
//        [tableView cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];
//        return;
//    }
    
    if(!lastindexpath)
    {
        lastindexpath=0;
    }
    if (![indexPath compare:lastindexpath] == NSOrderedSame)
    {
        [tableView cellForRowAtIndexPath:lastindexpath].contentView.backgroundColor = [UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];
    }
    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:25/255.0f green:100/255.0f blue:168/255.0f alpha:1.0];
    lastindexpath=indexPath;
#pragma for log out
    if(indexPath.row==9)
    {

        UIStoryboard *MainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        passwordViewController *nextView=[MainStoryboard instantiateViewControllerWithIdentifier:@"PasswordViewController"];
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        transition.type = kCATransitionFromRight; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
        //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
        [self.navigationController.view.layer addAnimation:transition forKey:nil];
        [self.navigationController pushViewController:nextView animated:YES];
    }
#pragma set bydefault color
    NSIndexPath *zeroindex=[NSIndexPath indexPathForRow:0 inSection:0];
    [tableView cellForRowAtIndexPath:zeroindex].contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0];
    // [tableView cellForRowAtIndexPath:zeroindex].contentView.backgroundColor = [UIColor redColor];
    NSIndexPath *seventhindex=[NSIndexPath indexPathForRow:10 inSection:0];
    [tableView cellForRowAtIndexPath:seventhindex].contentView.backgroundColor = [UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];

}

-(void)webservicelogout
{
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row!=10||indexPath.row!=0)
    {
    [tableView cellForRowAtIndexPath:indexPath].contentView.backgroundColor = [UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];
    }
    
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    //seguetoviewcontroller
    // Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
    {
       
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
        {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    else
    {
    }
}
// new code
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==9)
    {
    return 425.0;
    }
    else if(indexPath.row==0)
    {
        return 55.0;
    }
    return 45.0;
}

#pragma mark - RowSelectionDelegate Method
-(void)selectRowWithHeading:(int)index
{
    NSLog(@"THE CURRENT SELECTION IS: %d", index);
    lastindexpath=[NSIndexPath indexPathForRow:index inSection:0];;
   // NSLog(@"%ld",(long)lastindexpath.row);
    self.tableView.delegate=self;
   [self.tableView reloadData];
}
@end
