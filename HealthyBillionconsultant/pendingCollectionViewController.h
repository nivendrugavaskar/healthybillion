//
//  pendingCollectionViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 20/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface pendingCollectionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;

@end
