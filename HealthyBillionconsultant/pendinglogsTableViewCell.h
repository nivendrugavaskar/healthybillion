//
//  pendinglogsTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru Gavaskar on 20/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pendinglogsTableViewCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *customername;
@property (weak, nonatomic) IBOutlet UIImageView *arrowimage;
@property (weak, nonatomic) IBOutlet UIImageView *divider;

@end
