//
//  socialViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "socialViewController.h"

@interface socialViewController ()
{
    NSArray *socialarray;
    float cellheight;
}

@end

@implementation socialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma cell height
    socialarray = @[@"Cell1",@"Cell2",@"Cell3", @"Cell4"];
    cellheight=50.0;
    self.aTableview.layer.cornerRadius=5.0;
    if([socialarray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, [socialarray count]*cellheight);
    }
    else
    {
    self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, self.view.frame.size.height-self.aTableview.frame.origin.y-5);
    }
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
#pragma maintain scrroll view
    if([socialarray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.scrollEnabled=NO;
    }
    else
    {
        self.aTableview.scrollEnabled=YES;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"SOCIAL";
    [self setCustomNavigationButton];
    
    
}

#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
     [self.navigationController.navigationBar setCustomNavigationButton:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [socialarray objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    for (UIView *v in cell.contentView.subviews)
    {
       
        if(!([v isKindOfClass:[UIImageView class]]||[v isKindOfClass:[UILabel class]]))
        {
            //NSLog(@"tableview width=%f",self.aTableview.frame.size.width);
           // NSLog(@"view size=%f",v.frame.size.width);
          //  v.backgroundColor=[UIColor redColor];
            v.frame=CGRectMake((self.aTableview.frame.size.width-v.frame.size.width)/2, v.frame.origin.y, v.frame.size.width, v.frame.size.height);
        }
    }
    
    
    @try
    {
    
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [socialarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    //    [self performSegueWithIdentifier:@"segueToAddFoodDiary" sender:indexPath];
    
}

@end
