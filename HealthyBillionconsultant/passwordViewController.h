//
//  passwordViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface passwordViewController : UIViewController<UITextFieldDelegate>
{
    CGRect screenBounds,storerect;
    CGPoint pointtrack,pointtrackcontentoffset;
}
@property (strong, nonatomic) IBOutlet UITextField *txtPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtRepwd;
- (IBAction)didTapLogin:(id)sender;
- (IBAction)didTapclearData:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property(weak,nonatomic)UITextField *activeTextField;
@end
