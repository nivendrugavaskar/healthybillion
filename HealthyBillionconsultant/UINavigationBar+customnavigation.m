//
//  UINavigationBar+customnavigation.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 13/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "UINavigationBar+customnavigation.h"

@implementation UINavigationBar (customnavigation)



-(void)setCustomNavigationButton:(UIViewController *)vc
{
    
    if([vc isKindOfClass:[UITabBarController class]])
    {
    
    }
    
    vc.navigationController.navigationBar.backgroundColor=[UIColor clearColor];
    // self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:28/255.0f green:35/255.0f blue:37/255.0f alpha:0.08f];
    // self.navigationController.navigationBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"1242X65.png"]];
    [vc.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    // Set the gesture
    [vc.view addGestureRecognizer:vc.revealViewController.panGestureRecognizer];
    // Hide navigation bar and customize
    vc.navigationItem.hidesBackButton=YES;
    // new code
    //vc.navigationController.interactivePopGestureRecognizer.delegate = nil;
    
    // Set the gesture
    [vc.view addGestureRecognizer:vc.revealViewController.panGestureRecognizer];
    // new code
    hiderevealButton=[[UIButton alloc]initWithFrame:vc.view.bounds];
    [hiderevealButton addTarget:vc.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    hiderevealButton.backgroundColor=[UIColor clearColor];
    [vc.view addSubview:hiderevealButton];
    [hiderevealButton setHidden:YES];
    
    // create menu button
    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
    buttonImage.clipsToBounds=YES;
    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
    [Menubutton addTarget:vc.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [Menubutton addSubview:buttonImage];
    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
    vc.navigationItem.leftBarButtonItem=menubtn;
    // checking code
    UINavigationBar *navBar = [[vc navigationController] navigationBar];
    UIImage *backgroundImage = [UIImage imageNamed:@"1242X65.png"];
    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
}

@end
