//
//  customerDetailViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 14/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "customerDetailViewController.h"


@interface customerDetailViewController ()

@end

@implementation customerDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma accesing tabbar objects
     self.tabbarobj =(HBTabbarController *)self.tabBarController;
    // set text view frame
    _addresstxtview.frame=CGRectMake(_addresstxtview.frame.origin.x,_addresslabel.frame.origin.y-2, _addresstxtview.frame.size.width, _addresstxtview.frame.size.height);
    
    if(self.tabbarobj.sendarrayfromtab)
    {
        [self loadprofiledata];
    }
}
#pragma set data
-(void)loadprofiledata
{
    @try
    {
        _nametext.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"accountname"];
        _phntxt.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"phone"];
        _emailtxt.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"email1"];
        _addresstxtview.text=[NSString stringWithFormat:@"%@, %@",[self.tabbarobj.sendarrayfromtab valueForKey:@"account_billing_state"],[self.tabbarobj.sendarrayfromtab valueForKey:@"account_billing_country"]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"nill value");
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
