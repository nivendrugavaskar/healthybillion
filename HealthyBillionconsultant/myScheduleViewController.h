//
//  myScheduleViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "TKCalendarMonthViewController.h"
#import "TKCalendarMonthView.h"

@interface myScheduleViewController : UIViewController<TKCalendarMonthViewDataSource, TKCalendarMonthViewDelegate>
{
    AppDelegate *app;
    TKCalendarMonthView *calendar;
}

// new for tapku
@property (nonatomic,strong) TKCalendarMonthView *monthView;
@property(nonatomic, strong)NSArray *responseArr;
@property(nonatomic, strong)NSString *responseStr;

@end
