//
//  invoicesViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "invoicesViewController.h"

@interface invoicesViewController ()
{
    float cellheight;
    NSMutableArray *invoicearray;
}

@end

@implementation invoicesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma cell height
    invoicearray=[[NSMutableArray alloc] initWithObjects:@"Invoice 1",@"Invoice 2",@"Invoice 3", nil];
    
    cellheight=50.0;
    self.aTableview.layer.cornerRadius=5.0;
    if([invoicearray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
    self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, [invoicearray count]*cellheight);
    }
    else
    {
      self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, self.view.frame.size.height-self.aTableview.frame.origin.y-5);
    }
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
#pragma maintain scrroll view
    if([invoicearray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.scrollEnabled=NO;
    }
    else
    {
         self.aTableview.scrollEnabled=YES;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"INVOICES";
    [self setCustomNavigationButton];
    
    
}

#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
     [self.navigationController.navigationBar setCustomNavigationButton:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    invoicesTableViewCell *cell=(invoicesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    // set text colour and seperator colour
//    cell.foodName.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
//    cell.date.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
//    cell.seperatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    if(cell==nil)
    {
        cell=[[invoicesTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
        cell.datelabel.frame=CGRectMake(self.aTableview.frame.size.width-130,cell.datelabel.frame.origin.y, cell.datelabel.frame.size.width,cell.datelabel.frame.size.height);
        cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
        cell.arrowimageview.frame=CGRectMake(self.aTableview.frame.size.width-25, cell.arrowimageview.frame.origin.y, cell.arrowimageview.frame.size.width, cell.arrowimageview.frame.size.height);
//        
        cell.invoicename.text=[invoicearray objectAtIndex:indexPath.row];
//        cell.date.text=[[foodListingStoreArray objectAtIndex:indexPath.row] valueForKey:@"date"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [invoicearray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"pushtoinvoicedetail" sender:indexPath];
    
}


@end
