//
//  pendingCollectionViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 20/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "pendingCollectionViewController.h"
#import "pendingcollectionTableViewCell.h"

@interface pendingCollectionViewController ()
{
    NSMutableArray *collectionarray;
    float cellheight;
}

@end

@implementation pendingCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self.navigationController.navigationBar setCustomNavigationButton:self];
    
    cellheight=40.0f;
    collectionarray=[[NSMutableArray alloc] initWithObjects:@"Collection1",@"Collection2",@"Collection3", nil];
    
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    self.aTableview.layer.cornerRadius=5.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    pendingcollectionTableViewCell *cell=(pendingcollectionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    
    if(cell==nil)
    {
        cell=[[pendingcollectionTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    
    @try
    {
        
        cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
//        
        cell.name.text=[collectionarray objectAtIndex:indexPath.row];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [collectionarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}





@end
