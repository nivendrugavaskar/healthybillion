//
//  pendinglogsViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru Gavaskar on 20/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "pendinglogsTableViewCell.h"

@interface pendinglogsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *app;
}
@property (weak, nonatomic) IBOutlet UITableView *aTableview;

@end
