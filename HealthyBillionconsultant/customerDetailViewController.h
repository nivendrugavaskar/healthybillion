//
//  customerDetailViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 14/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HBTabbarController.h"

@interface customerDetailViewController : UIViewController
{
    AppDelegate *app;
}

@property (strong, nonatomic) IBOutlet UIScrollView *aScrollview;
@property (strong, nonatomic) IBOutlet UIView *imgviewinnerview;
@property (strong, nonatomic) IBOutlet UIImageView *profileimg;
@property (strong, nonatomic) IBOutlet UILabel *addresslabel;
@property (strong, nonatomic) IBOutlet UITextField *nametext;
@property (strong, nonatomic) IBOutlet UITextField *phntxt;
@property (strong, nonatomic) IBOutlet UITextField *emailtxt;
@property (strong, nonatomic) IBOutlet UITextView *addresstxtview;

@property(nonatomic,strong)NSMutableArray *dataArray;
@property HBTabbarController *tabbarobj;

@end
