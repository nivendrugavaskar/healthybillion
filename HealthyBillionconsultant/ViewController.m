//
//  ViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    UIView *paddingView;
    AppDelegate *app;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    // Do any additional setup after loading the view, typically from a nib.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    pointtrackcontentoffset=self.view.frame.origin;
    storerect=self.view.frame;
    screenBounds = [[UIScreen mainScreen] bounds];
#pragma add padding
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtphoneno.leftView = paddingView;
    _txtphoneno.leftViewMode = UITextFieldViewModeAlways;
#pragma hide label initially
    _inboxLabel.hidden=YES;
}
#pragma textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack =textField.frame.origin;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
#pragma Animate View
    CGRect rect=storerect;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(screenBounds.size.height==480)
    {
        rect.origin.y-=pointtrack.y-180;
    }
    else if(screenBounds.size.height==568)
    {
        rect.origin.y-=pointtrack.y-180;
    }
    else if(screenBounds.size.height==667)
    {
         rect.origin.y-=20;
    }
    else if(screenBounds.size.height==736)
    {
        rect.origin.y-=20;
    }
    else
    {
       rect.origin.y-=pointtrack.y-180;
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismisskeyboard];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if([textField.text length]>12 && range.length == 0)
    {
        return NO;
    }
    return YES;
}

- (IBAction)didTapAccessKey:(id)sender
{
    [self dismisskeyboard];
#pragma Manage buttons action
    if([_btnaccesskey.titleLabel.text isEqualToString:@"Submit"])
    {
        [self webserviceaccespage1stone];
        
    }
    else
    {
#pragma calling service after enter phone number
        if([NSString validation:_txtphoneno.text])
        {
           [self webservicephoneenter];
        }
        else
        {
           UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Phone number mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           [alert show];
        }
        [self.activeTextField resignFirstResponder];
    }
    
}
-(void)dismisskeyboard
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    //[[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextField resignFirstResponder];
}

#pragma Webservice for phone number authenticating
-(void)webservicephoneenter
{
    
    NSString *path =[NSString stringWithFormat:@"%@getaccesskey",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:_txtphoneno.text,@"phone",@"H9",@"user_type",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Authenticating.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             _txtphoneno.text=@"";
             
         }
         else
         {
             _txtphoneno.text=@"";
             _inboxLabel.hidden=NO;
             _txtphoneno.placeholder=@"Enter Access Key";
             [_btnaccesskey setTitle:@"Submit" forState:UIControlStateNormal];
#pragma store username
             app->customerusername=[[dict valueForKey:@"result"] valueForKey:@"username"];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}
#pragma Webservice for accesskey authenticating 1stone
-(void)webserviceaccespage1stone
{
    
    NSString *path =[NSString stringWithFormat:@"%@getchallenge",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customerusername,@"username",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
          NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             
         }
         else
         {
             app->customertoken=[[dict valueForKey:@"result"] valueForKey:@"token"];
             app->customeraccesskey=_txtphoneno.text;
             [self webserviceaccespage2ndone];
            // [self performSegueWithIdentifier:@"pushTopassword" sender:self];
         }
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}
#pragma Webservice for accesskey authenticating 2ndone
-(void)webserviceaccespage2ndone
{
    NSString *myString = [NSString stringWithFormat:@"%@%@",app->customertoken,app->customeraccesskey];
    NSString *md5 = [NSString md5HexDigest:myString];
    NSString *path =[NSString stringWithFormat:@"%@login",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customerusername,@"username",md5,@"accessKey",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             app->customersessionname=[[dict valueForKey:@"result"] valueForKey:@"sessionName"];
             [self performSegueWithIdentifier:@"pushTopassword" sender:self];
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
