//
//  invoiceDetailsViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 19/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "invoiceDetailsViewController.h"
#import "invoicelistTableViewCell.h"

@interface invoiceDetailsViewController ()
{
    NSMutableArray *itemarray;
    float cellheight;
}

@end

@implementation invoiceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title=@"INVOICES DETAILS";
    cellheight=44.0f;
    itemarray=[[NSMutableArray alloc] initWithObjects:@"Item1",@"Item2",@"Item3",@"Item4",@"Item5",@"Item6",@"Item7",@"Item8", nil];
    
    self.navigationItem.hidesBackButton=TRUE;
    
    // add back button
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItems = @[backbarbutton];
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    invoicelistTableViewCell *cell=(invoicelistTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    // set text colour and seperator colour
    //    cell.foodName.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    //    cell.date.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    //    cell.seperatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    if(cell==nil)
    {
        cell=[[invoicelistTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
        //cell.datelabel.frame=CGRectMake(self.aTableview.frame.size.width-130,cell.datelabel.frame.origin.y, cell.datelabel.frame.size.width,cell.datelabel.frame.size.height);
        //cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
        //cell.arrowimageview.frame=CGRectMake(self.aTableview.frame.size.width-25, cell.arrowimageview.frame.origin.y, cell.arrowimageview.frame.size.width, cell.arrowimageview.frame.size.height);
        //
        cell.itemname.text=[itemarray objectAtIndex:indexPath.row];
        //        cell.date.text=[[foodListingStoreArray objectAtIndex:indexPath.row] valueForKey:@"date"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [itemarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
}

- (UIView *)  tableView:(UITableView *)tableView
 viewForFooterInSection:(NSInteger)section{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.aTableview.frame.size.width, 30)];
    footerView.backgroundColor=[UIColor colorWithRed:217/255.0f green:245/255.0f blue:250/255.0f alpha:1.0f];
    footerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UILabel *total = nil;
    UILabel *totalvalue = nil;
    UIImageView *imgview=nil;
    
    if ([tableView isEqual:self.aTableview] &&
        section == 0)
    {
        imgview=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, self.aTableview.frame.size.width, 3)];
        imgview.backgroundColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0f];
        [footerView addSubview:imgview];
        
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(2,7, 100, 20)];
        total.font=[UIFont systemFontOfSize:14.0f];
        total.textColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0f];
        total.text = @"Total";
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        totalvalue = [[UILabel alloc] initWithFrame:CGRectMake(self.aTableview.frame.size.width-148,7, 140, 20)];
        totalvalue.textAlignment=NSTextAlignmentRight;
        totalvalue.font=[UIFont systemFontOfSize:14.0f];
        totalvalue.textColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0f];
        totalvalue.text = @"INR 5000";
        totalvalue.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:totalvalue];
        
    }
    
    return footerView;
    
}

@end
