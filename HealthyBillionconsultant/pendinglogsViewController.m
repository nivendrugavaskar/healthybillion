//
//  pendinglogsViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru Gavaskar on 20/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "pendinglogsViewController.h"

@interface pendinglogsViewController ()
{
    NSMutableArray *pendinglogsarray,*sectionarray;
    float cellheight;
}

@end

@implementation pendinglogsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self.navigationController.navigationBar setCustomNavigationButton:self];
    self.title=@"PENDING LOGS";
    
    cellheight=40.0f;
    pendinglogsarray=[[NSMutableArray alloc] initWithObjects:@"Customername1-session1",@"Customername2-session2",@"Customername3-session3",nil];
    sectionarray=[[NSMutableArray alloc] initWithObjects:@"20 Dec 2014",@"22 Dec 2014",@"24 Dec 2015",nil];
    
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    self.aTableview.layer.cornerRadius=5.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    pendinglogsTableViewCell *cell=(pendinglogsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    
    if(cell==nil)
    {
        cell=[[pendinglogsTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    
    @try
    {

        cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
        
        cell.customername.text=[pendinglogsarray objectAtIndex:indexPath.row];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [pendinglogsarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionarray count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, tableView.frame.size.width, 16)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    NSString *string =[sectionarray objectAtIndex:section];
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    [view sizeToFit];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
