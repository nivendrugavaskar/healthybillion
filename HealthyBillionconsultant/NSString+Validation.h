//
//  NSString+Validation.h
//  Belinda
//
//  Created by Nivendru on 27/11/14.
//  Copyright (c) 2014 NivendruGavaskar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validation)
+(BOOL)validation:(NSString *)sendstring;
- (NSString *)MD5String;
+ (NSString*)md5HexDigest:(NSString*)input;
@end
