//
//  invoicesTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface invoicesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *invoicename;
@property (strong, nonatomic) IBOutlet UILabel *datelabel;
@property (strong, nonatomic) IBOutlet UIImageView *arrowimageview;
@property (strong, nonatomic) IBOutlet UILabel *divider;

@end
