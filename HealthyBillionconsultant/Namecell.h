//
//  Namecell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 07/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Namecell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
