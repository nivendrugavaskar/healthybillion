//
//  pendingcollectionTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 20/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pendingcollectionTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *divider;
@property (strong, nonatomic) IBOutlet UIImageView *arrow;

@end
