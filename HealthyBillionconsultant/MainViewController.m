//
//  MainViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 06/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "MainViewController.h"
#import "SidebarViewController.h"


@interface MainViewController ()<SWRevealViewControllerDelegate>
{
    BOOL editingmode;
    UIButton *editbtn;
    UIImageView *buttonimageview;
    int currentField;
    CGRect screenBounds;
    CGPoint pointtrack,pointtrackcontentoffset;
    CGSize kbsize,contentsize;
    UIView *paddingView;
}

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma set delegate for reveal
    self.rowSelectionDelegate = (SidebarViewController *)self.revealViewController.rearViewController;
#pragma store bydefault value
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    screenBounds = [[UIScreen mainScreen] bounds];
    contentsize=self.aScrollview.contentSize;
    
#pragma maintain keyboard notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
#pragma  by default false boolean
    
    editingmode=FALSE;
    
#pragma apply padding
    for (id txt in self.aScrollview.subviews)
    {
        if([txt isKindOfClass:[UITextField class]])
        {
            UITextField *local=(UITextField *)txt;
            paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            local.leftView = paddingView;
            local.leftViewMode = UITextFieldViewModeAlways;
        }
    }
#pragma made round image view
    _profileImgview.layer.cornerRadius=_profileImgview.frame.size.width/2;
    
    // set textview allignment
    self.txtviewAddress.frame=CGRectMake(self.txtviewAddress.frame.origin.x, self.addresslabel.frame.origin.y-2, self.txtviewAddress.frame.size.width, self.txtviewAddress.frame.size.height);
}
#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
//    UIButton *Menubutton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 30, 28)];
//    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 5, 20, 18)];
//    buttonImage.image=[UIImage imageNamed:@"menu_icon.png" ];
//    buttonImage.clipsToBounds=YES;
//    buttonImage.contentMode=UIViewContentModeScaleAspectFit;
//    [Menubutton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//    [Menubutton addSubview:buttonImage];
//    UIBarButtonItem *menubtn=[[UIBarButtonItem alloc]initWithCustomView:Menubutton];
//    self.navigationItem.leftBarButtonItem=menubtn;
//    // checking code
//    UINavigationBar *navBar = [[self navigationController] navigationBar];
//    UIImage *backgroundImage = [UIImage imageNamed:@"1242X65.png"];
//    [navBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setCustomNavigationButton:self];
#pragma adding editiable button
    
    editbtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 46, 44)];
    [editbtn addTarget:self action:@selector(editable) forControlEvents:UIControlEventTouchUpInside];
    //editbtn.backgroundColor=[UIColor redColor];
    buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(13,12, 20, 20)];
   // buttonimageview.backgroundColor=[UIColor yellowColor];
    buttonimageview.image=[UIImage imageNamed:@"edit_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFill;
    [editbtn addSubview:buttonimageview];
    
    UIBarButtonItem *editablebarbtn = [[UIBarButtonItem alloc] initWithCustomView:editbtn];
    editablebarbtn.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItems = @[editablebarbtn];
    
}
// editable method
-(void)editable
{
  if(!editingmode)
  {
      editingmode=TRUE;
      buttonimageview.image=[UIImage imageNamed:@"save_icon.png"];
      for (id txt in self.aScrollview.subviews)
        {
             if([txt isKindOfClass:[UITextField class]])
              {
                  UITextField *local=(UITextField *)txt;
                  local.layer.borderColor=[UIColor blackColor].CGColor;
                  local.layer.borderWidth=1.0;
                  local.userInteractionEnabled=YES;
              }
              else if([txt isKindOfClass:[UITextView class]])
              {
                  UITextView *local=(UITextView *)txt;
                  local.layer.borderColor=[UIColor blackColor].CGColor;
                  local.layer.borderWidth=1.0;
                  local.userInteractionEnabled=YES;
              }
            
        }
#pragma camera btn manage
      _photoeditimage.hidden=FALSE;
      _btnSelectpic.userInteractionEnabled=YES;
      
  }
 else
 {
      editingmode=FALSE;
      buttonimageview.image=[UIImage imageNamed:@"edit_icon.png"];
     for (id txt in self.aScrollview.subviews)
     {
         if([txt isKindOfClass:[UITextField class]])
         {
             UITextField *local=(UITextField *)txt;
             local.layer.borderColor=[UIColor clearColor].CGColor;
             local.layer.borderWidth=0.0;
             local.userInteractionEnabled=NO;
         }
         else if([txt isKindOfClass:[UITextView class]])
         {
             UITextView *local=(UITextView *)txt;
             local.layer.borderColor=[UIColor clearColor].CGColor;
             local.layer.borderWidth=0.0;
             local.userInteractionEnabled=NO;
         }
         
     }
#pragma camera btn manage
     _photoeditimage.hidden=TRUE;
     _btnSelectpic.userInteractionEnabled=NO;
 }
}
#pragma added observer selector
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    kbsize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.aScrollview setContentSize:CGSizeMake(self.view.bounds.size.width,self.aScrollview.frame.size.height+kbsize.height)];
    
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.aScrollview setContentOffset:pointtrackcontentoffset animated:NO];
    [self.aScrollview setContentSize:contentsize];
}

#pragma textfield and textview delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentField=0;
    self.activeTextfield=textField;
    self.activeTextview=nil;
    pointtrack = textField.frame.origin;
    
#pragma add toolbar
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1000;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
#pragma add accessory
    [_txtName setInputAccessoryView:toolBar];
    [_txtEmail setInputAccessoryView:toolBar];
    [_txtPhone setInputAccessoryView:toolBar];

    if(screenBounds.size.height==480)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-30) animated:YES];
    }
    else if(screenBounds.size.height==568)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-80) animated:YES];
    }
    else if(screenBounds.size.height==667)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-136) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-173) animated:YES];
    }
    else
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-300) animated:YES];
    }
}
-(void)dismisskeyboard
{
    if(currentField==0)
    {
    [[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextfield resignFirstResponder];
    }
    else
    {
    [[self.view viewWithTag:1001] removeFromSuperview];
    [self.activeTextview resignFirstResponder];
    }
   
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(currentField==0)
    {
    
        long tag=self.activeTextfield.tag;
        tag++;
        UITextField *txt=(UITextField *)[self.view viewWithTag:tag];
        if(txt==nil)
        {
            [self.activeTextfield resignFirstResponder];
        }
        else
        {
           [txt becomeFirstResponder];
        }
    }
    else if(currentField==1)
    {
        
        long tag=self.activeTextview.tag;
        tag++;
        UITextView *txt=(UITextView *)[self.view viewWithTag:tag];
        if(txt==nil)
        {
            //[self.activeTextview resignFirstResponder];
        }
        else
        {
            [txt becomeFirstResponder];
        }
    }
   
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    currentField=1;
    self.activeTextview=textView;
    self.activeTextfield=nil;
    pointtrack = textView.frame.origin;
#pragma add toolbar
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1001;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
#pragma add accessory
    [_txtviewAddress setInputAccessoryView:toolBar];
    [_txtviewAddress reloadInputViews];
    
    if(screenBounds.size.height==480)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-30) animated:YES];
    }
    else if(screenBounds.size.height==568)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-80) animated:YES];
    }
    else if(screenBounds.size.height==667)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-136) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-173) animated:YES];
    }
    else
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-300) animated:YES];
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.rowSelectionDelegate selectRowWithHeading:0];
    [self.rowSelectionDelegate selectRowWithHeading:1];
    self.title = @"PROFILE";
   [self setCustomNavigationButton];
//    
//#pragma Load user data
    [self loaddata];
    
}
-(void)loaddata
{
    NSLog(@"%@",[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"]);
    NSString *append=[NSString stringWithFormat:@" %@",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"last_name"]];
    _txtName.text=[[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"first_name"] stringByAppendingString:append];
    _txtPhone.text=[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"phone_mobile"];
    _txtEmail.text=[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"email1"];
#pragma manage address as shown
    NSString *mainaddress=[NSString stringWithFormat:@"%@, %@ -%@, %@",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_street"],[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_city"],[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_postalcode"],[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_state"]];
    _txtviewAddress.text=mainaddress;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapimageedit:(id)sender
{
    [self dismisskeyboard];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    alert.tag=100;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==1)
        {
            [self takepicture];
        }
        else if(buttonIndex==2)
        {
            [self selectgallery];
        }
    }
    
}
-(void)selectgallery
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:nil];
        NSLog(@"camera");
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    selectedimage=[UIImage scaleAndRotateImage:selectedimage];
    _profileImgview.contentMode=UIViewContentModeScaleAspectFill;
    _profileImgview.image=selectedimage;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapChangepwd:(id)sender
{
    NSLog(@"clicked");
    [self createcustomviewpwd];
}
#pragma create customview
-(void)createcustomviewpwd
{
    if(editingmode)
    {
        [self editable];
    }
    
    if([self.view viewWithTag:10000])
    {
        return;
    //[[self.view viewWithTag:10000] removeFromSuperview];
    }
    UIView *changepwdview=[[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-20,100,300,280)];
    changepwdview.tag=10000;
    CGRect bounds = self.view.bounds;
    changepwdview.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    changepwdview.backgroundColor=[UIColor lightGrayColor];
    UITextField *txt1=[[UITextField alloc] initWithFrame:CGRectMake(5, 60, 290, 30)];
    txt1.tag=5;
    txt1.delegate=self;
    txt1.borderStyle=UITextBorderStyleLine;
    txt1.placeholder=@"Enter Old Password";
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txt1.leftView = paddingView;
    txt1.leftViewMode = UITextFieldViewModeAlways;
    [txt1 setFont:[UIFont systemFontOfSize:14.0]];
    
    UITextField *txt2=[[UITextField alloc] initWithFrame:CGRectMake(5, 110, 290, 30)];
    txt2.tag=6;
    txt2.delegate=self;
    txt2.borderStyle=UITextBorderStyleLine;
    txt2.placeholder=@"Enter New Password";
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txt2.leftView = paddingView;
    txt2.leftViewMode = UITextFieldViewModeAlways;
    [txt2 setFont:[UIFont systemFontOfSize:14.0]];
    
    UITextField *txt3=[[UITextField alloc] initWithFrame:CGRectMake(5, 160, 290, 30)];
    txt3.tag=7;
    txt3.delegate=self;
    txt3.borderStyle=UITextBorderStyleLine;
    txt3.placeholder=@"Re-enter Password";
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txt3.leftView = paddingView;
    txt3.leftViewMode = UITextFieldViewModeAlways;
    [txt3 setFont:[UIFont systemFontOfSize:14.0]];
    
    [changepwdview addSubview:txt1];
    [changepwdview addSubview:txt2];
    [changepwdview addSubview:txt3];
    UIButton *cancel=[UIButton buttonWithType:UIButtonTypeCustom];
    cancel.frame=CGRectMake(10, 240, 130, 30);
    cancel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"button.png"]];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(dismissview) forControlEvents:UIControlEventTouchUpInside];
    UIButton *save=[UIButton buttonWithType:UIButtonTypeCustom];
    save.frame=CGRectMake(155, 240, 130, 30);
    save.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"button.png"]];
    [save setTitle:@"Save" forState:UIControlStateNormal];
    
    [changepwdview addSubview:cancel];
    [changepwdview addSubview:save];
    //[self.view addSubview:changepwdview];
    
    [UIView transitionWithView:self.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        [self.view addSubview:changepwdview];
                    } completion:nil];
    
}

-(void)dismissview
{
    [[self.view viewWithTag:10000] removeFromSuperview];
}

#pragma remove observer
-(void)dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
