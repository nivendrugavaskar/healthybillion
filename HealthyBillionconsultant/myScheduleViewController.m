//
//  myScheduleViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//
#import <CoreGraphics/CoreGraphics.h>
#import "myScheduleViewController.h"



// new for tapku
static int calendarShadowOffset = (int)-20;


@interface myScheduleViewController ()

@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *disabledDates;


// new for tapku
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;


@end

@implementation myScheduleViewController
{
    // new for tapku
    NSMutableArray *selectedDateArr;
    NSString *selectedDateStr;
     NSMutableArray *serviceDateArr;
    NSDateFormatter *df;

}

// for tapku
@synthesize responseArr, responseStr;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // for tapku
    selectedDateArr = [[NSMutableArray alloc]init];
    self.dataDictionary = [[NSMutableDictionary alloc]init];
    
    df = [[NSDateFormatter alloc]init];
    
//    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-GB"];
//    [df setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    [df  setDateFormat:@"yyyy-mm-dd"];
    
    for (int i =0; i < [responseArr count]; i++) {
        
        NSString *key = [NSString stringWithFormat:@"%@ 00:00:00 +0000", [responseArr objectAtIndex:i]];
        
        [selectedDateArr addObject: key];
    }
    
    
    [self createCalendarView];
    [self showHideCalendarView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"MY SCHEDULE";
//    self.navigationController.navigationBar.backgroundColor=[UIColor clearColor];
//    // self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:28/255.0f green:35/255.0f blue:37/255.0f alpha:0.08f];
//    // self.navigationController.navigationBar.barTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"1242X65.png"]];
//    [self.navigationController.navigationBar
//     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
//    // Set the gesture
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    // Hide navigation bar and customize
//    self.navigationItem.hidesBackButton=YES;
//    // Set the gesture
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    // new code
//    hiderevealButton=[[UIButton alloc]initWithFrame:self.view.bounds];
//    [hiderevealButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//    hiderevealButton.backgroundColor=[UIColor clearColor];
//    [self.view addSubview:hiderevealButton];
//    [hiderevealButton setHidden:YES];
    [self setCustomNavigationButton];
    
    
}

#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
   [self.navigationController.navigationBar setCustomNavigationButton:self];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


// for tapku
-(void)showHideCalendarView
{
    
    
    [calendar reload];
    
    // If calendar is off the screen, show it, else hide it (both with animations)
    if (calendar.frame.origin.y == self.view.frame.size.height + calendar.frame.size.height+calendarShadowOffset)
    {
        // Show
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.75];
        if ([[UIScreen mainScreen] bounds].size.height == 480) {
            
            calendar.frame = CGRectMake(0, 60, calendar.frame.size.width , calendar.frame.size.height);
        }else {
            
            calendar.frame = CGRectMake(0, 60, calendar.frame.size.width , calendar.frame.size.height);
        }
        
        [UIView commitAnimations];
    } else {
        // Hide
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.75];
        calendar.frame = CGRectMake(0, self.view.frame.size.height + calendar.frame.size.height+calendarShadowOffset, calendar.frame.size.width, calendar.frame.size.height);
        [UIView commitAnimations];
    }
}

-(void)createCalendarView
{
    calendar = 	[[TKCalendarMonthView alloc] init];
    
    calendar.backgroundColor = [UIColor whiteColor];
    calendar.delegate = self;
    calendar.dataSource = self;
    
    // Add Calendar to just off the top of the screen so it can later slide down
    calendar.frame = CGRectMake(0, self.view.frame.size.height + calendar.frame.size.height+calendarShadowOffset, calendar.frame.size.width, calendar.frame.size.height);
    // Ensure this is the last "addSubview" because the calendar must be the top most view layer
    [self.view addSubview:calendar];
    [calendar reload];
}

#pragma  mark calendar delegate

- (void)calendarMonthView:(TKCalendarMonthView *)monthView monthDidChange:(NSDate *)d {
    
    //   [super calendarMonthView:monthView monthDidChange:d animated:YES];
    NSLog(@"calendarMonthView monthDidChange");
    
    
}
#pragma  mark - calendar data source

-(NSArray *)calendarMonthView:(TKCalendarMonthView *)monthView marksFromDate:(NSDate *)startDate toDate:(NSDate *)lastDate
{
    
    
    [self generateRandomDataForStartDate:startDate endDate:lastDate];
    return self.dataArray;
    
}

-(void)calendarMonthView:(TKCalendarMonthView *)monthView didSelectDate:(NSDate *)d
{
    // [self.calendarTableView reloadData];
    
    df = [[NSDateFormatter alloc]init];
    
    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-GB"];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSLog(@" date_Selected %@", [calendar dateSelected]);
    
    NSDate * calendarDate = [calendar dateSelected];
    
    
    
    if ([selectedDateArr containsObject:[d description]])
    {
        // hide now
        //[SVProgressHUD showWithStatus:@"Please wait"];
        [df  setDateFormat:@"yyyy-MM-dd"];
        
        selectedDateStr = [df stringFromDate:calendarDate];
        
        // [self performSelector:@selector(selectedDateService) withObject:nil afterDelay:0.5];
    }
    
    
    
    
    [df  setDateFormat:@"yyyy-MM-dd"];
    
    /*   alert = [[UIAlertView alloc]initWithTitle:@"Selected Date" message:[df stringFromDate:calendarDate] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];
     
     alert = [[UIAlertView alloc]initWithTitle:@"Calendar Description" message:[d description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];
     
     alert = [[UIAlertView alloc]initWithTitle:@"Green Date" message:[ NSString stringWithFormat:@"%@", selectedDateArr] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];
     */
    
}


- (void) generateRandomDataForStartDate:(NSDate*)startDate endDate:(NSDate*)lastDate{
    
    //    NSArray *data = [NSArray arrayWithObjects:
    //                     @"2014-05-10 00:00:00 +0000", @"2011-01-09 00:00:00 +0000", @"2011-01-22 00:00:00 +0000", nil];
    
    
    // Initialise empty marks array, this will be populated with TRUE/FALSE in order for each day a marker should be placed on.
    //	NSMutableArray *marks = [NSMutableArray array];
    self.dataArray = [NSMutableArray array];
    //	self.dataDictionary = [NSMutableDictionary dictionary];
    // new code
    selectedDateArr=[[NSMutableArray alloc] initWithObjects:@"2014-05-23 00:00:00 +0000",@"2015-01-22 00:00:00 +0000",@"2015-01-15 00:00:00 +0000", nil];
    //service:2014-05-23 00:00:00 +0000,
    //    2014-05-15 00:00:00 +0000,
    //    2014-08-08 00:00:00 +0000,
    //    2014-08-09 00:00:00 +0000,
    //    2014-08-10 00:00:00 +0000,
    //    2014-08-12 00:00:00 +0000,
    //    2014-08-21 00:00:00 +0000,
    //    2014-10-22 00:00:00 +0000,
    //    2014-10-30 00:00:00 +0000,
    //    2014-11-25 00:00:00 +0000,
    //    2014-12-05 00:00:00 +0000,
    //    2014-12-18 00:00:00 +0000,
    //    2015-01-22 00:00:00 +0000,
    //    2015-01-17 00:00:00 +0000,
    //    2015-01-10 00:00:00 +0000,
    //    2015-01-18 00:00:00 +0000,
    
    // Initialise calendar to current type and set the timezone to never have daylight saving
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSDateComponents *comp = [cal components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit |
                                              NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit)
                                    fromDate:startDate];
    
    NSDate *d = [cal dateFromComponents:comp];
    
    // Init offset components to increment days in the loop by one each time
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:1];
    
    
    // for each date between start date and end date check if they exist in the data array
    while (YES) {
        
        // Is the date beyond the last date? If so, exit the loop.
        // NSOrderedDescending = the left value is greater than the right
        if ([d compare:lastDate] == NSOrderedDescending) {
            break;
        }
        
        // If the date is in the data array, add it to the marks array, else don't
        if ([selectedDateArr containsObject:[d description]]) {
            
            
            
            //     (self.dataDictionary)[d] = @[@"Item one",@"Item two"];
            [self.dataArray addObject:@YES];
            
        } else {
            [self.dataArray addObject:[NSNumber numberWithBool:NO]];
        }
        
        
        
        // Increment day using offset components (ie, 1 day in this instance)
        d = [cal dateByAddingComponents:offsetComponents toDate:d options:0];
        
    }
    
    
}





@end
